Роль для установки patroni ansible

Файл consul.json
1. Поменять строку "encrypt": "CHAHNGEIT", необходимо сгенерировать командой consul keygen 
2. "start_join": [
    "CHAHNGEIT"  ## ip адрес первого мастер-сервера
  ],
3.  "recursors": [
    "XX.XX.XX.XX",  ## Указать DNS
    "XX.XX.XX.XX"   ##
  ],

Файл postgres.yml
секция ph_hba
 - host    all             postgres        XX.XX.XX.XX/32         trust
 - host    replication     postgres        XX.XX.XX.XX/32         trust
Добавить ноды patroni

Patroni ansible installation role

Consul.json file
1. Change the line "encrypt": "CHAHNGEIT", you need to generate the command consul keygen.
2. "start_join": [
     "CHAHNGEIT" ## ip address of the first master server
   ],
3. "recursors": [
     "XX.XX.XX.XX", ## Specify DNS
     "XX.XX.XX.XX" ##
   ],

Postgres.yml file
ph_hba section
  - host all postgres XX.XX.XX.XX / 32 trust
  - host replication postgres XX.XX.XX.XX / 32 trust
Add patroni nodes