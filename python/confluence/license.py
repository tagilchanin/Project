from pyvirtualdisplay import Display
display = Display(visible=0, size=(800, 600))
display.start()

# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

##LOGIN SITE
driver = webdriver.Firefox()
driver.get("https://id.atlassian.com/login?application=mac&continue=https://my.atlassian.com")
driver.find_element_by_id("username").send_keys("MYNAME@MYDOMAIN.RU")
driver.find_element_by_id("form-login").submit()
time.sleep(20)
driver.find_element_by_id("login-submit").click()
driver.find_element_by_id("password").clear()
driver.find_element_by_id("password").send_keys("MYPASSWORD")
driver.find_element_by_id("login-submit").click()
time.sleep(20)

# GENERATE LICENSE
driver.find_element_by_link_text("New Evaluation License").click()
driver.find_element_by_id("product-select").click()
Select(driver.find_element_by_id("product-select")).select_by_visible_text("Confluence")
driver.find_element_by_xpath("//option[@value='Confluence']").click()
driver.find_element_by_xpath("//div[@id='content']/div/div/div/form/fieldset/div[2]/div/label[2]/span[3]").click()
driver.find_element_by_id("serverid").click()
driver.find_element_by_id("serverid").clear()
driver.find_element_by_id("serverid").send_keys("SERVERID")
driver.find_element_by_id("generate-license").click()

# SEARCH AND COPY LICENSE
time.sleep(20)
#
elem = driver.find_element_by_xpath("//textarea[contains(@id, 'licensebox')]").text

## ADD NEW LICENSE TO CONFLUENCE

driver.get(
    "http://confluence.MYDOMAIN.ru/login.action;jsessionid=B15901EC01DB92F82DBE762FC4727AA7?os_destination=%2Findex.action")
time.sleep(10)
driver.find_element_by_id("os_username").clear()
driver.find_element_by_id("os_username").send_keys("MYNAME")
driver.find_element_by_id("os_password").clear()
driver.find_element_by_id("os_password").send_keys("MYPASSWORD")
driver.find_element_by_name("loginform").submit()
time.sleep(15)
driver.find_element_by_id("admin-menu-link").click()
driver.find_element_by_id("administration-link").click()
driver.find_element_by_id("password").click()
driver.find_element_by_id("password").clear()
driver.find_element_by_id("password").send_keys("MYPASSWORD")
time.sleep(15)
driver.find_element_by_name("authenticateform").submit()
time.sleep(5)
driver.find_element_by_link_text("License Details").click()
time.sleep(5)
driver.find_element_by_name("licenseString").send_keys(elem)
driver.find_element_by_name("update").click()

driver.quit()
display.quit()