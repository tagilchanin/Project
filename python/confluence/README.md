Скрипт автоматического продления лицензии для conflunce
Для работы не обходимы установить:
1. pip install selenium
2. pip install pyvirtualdisplay
3. скопировать geckodriver в /usr/local/bin
4. установить firefox не ниже версии 59.
5. установить xorg-x11-server-Xvfb


Automatic license renewal script for conflunce
To work, you need to install:
1. pip install selenium
2. pip install pyvirtualdisplay
3. Copy geckodriver to /usr/local/bin
4. Install firefox version 59 or higher.
5. Install xorg-x11-server-Xvfb